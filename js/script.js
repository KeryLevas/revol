var FPS = 30;
var gameR = parseInt($("#moveBox").width());
var gameL = parseInt($("#moveBox").css("left"));
var gravity = -20;
var KEY = { 
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39,
    SPACE: 32
};


var hero={
    health:3,
    speed:10,
    jumpSpeed:220,
    pressedJump:false,
    move : function() {
    var border_left = parseInt($("#movebox").css("width")) - 1070;
    var border_right = parseInt($("#movebox").css("width")) - 130;
    var left = parseInt($("#hero").css("left"));

        if (game.pressedKeys[KEY.LEFT]) {
            $("#hero").css("left",left-hero.speed);
            if (left - border_left < 0) {
                hero.speed = 0;
                $("#hero").css("left",left-hero.speed);
                hero.speed = 10;
            }
        }

        if (game.pressedKeys[KEY.RIGHT]) {
            $("#hero").css("left",left+hero.speed);
            if (left > border_right) {
                hero.speed = 0;
                $("#hero").css("left",left+hero.speed);
                hero.speed = 10;
            }
        }

        if (game.pressedKeys[KEY.UP]) {
            if(pressedJump != true) {
                pressedJump = true;
                var top = parseInt($("#hero").css("top"));
                $("#hero").css("top",top-hero.jumpSpeed);
            }
        }
    },

    update : function() {
        var top = parseInt($("#hero").css("top"));
        $("#hero").css("top",top-gravity);
        if (top > 220) {

            pressedJump = false;
            gravity = 0;
            $("#hero").css("top",top);
            gravity = -20;

        }
    }
}

var berkut = {
	speed:10,
	move : function() {
		var AIleft = parseInt($(".berkut").css("left"));
		$(".berkut").css("left",AIleft-berkut.speed);
		if (AIleft < -100) {
			AIleft = 1000;
			$(".berkut").css("left",AIleft-berkut.speed);
		}
	}
};

var game = {};
game.pressedKeys = []; // array of pressed keys for synchron input

$(document).ready(function(){

    game.timer = setInterval(gameloop,1000/FPS);
});

$(document).keydown(function(e){
    game.pressedKeys[e.which] = true;
});

$(document).keyup(function(e){
    game.pressedKeys[e.which] = false;
});

function gameloop() {
	hero.move();
	hero.update();
	berkut.move();
}


